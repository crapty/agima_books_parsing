# README #

Для запуска выполните команду
`docker-compose -f docker-compose.prod.yml up -d`

Использование
==

1. Форма для загрузки книг доступна по адресу http://localhost:7780/catalog/upload
1. API эндпоинты:
    1. Загрузка книг - `POST http://localhost:7780/api/v1/catalog/upload`
    1. Количество загруженных книг по дням - `GET http://localhost:7780/api/v1/catalog/books-per-day`
    1. Количество загруженных книг по авторам - `GET http://localhost:7780/api/v1/catalog/books-per-author`
    1. Список авторов без книг - `GET http://localhost:7780/api/v1/catalog/authors-without-books`
1. Для загрузки книг через консоль используйте следующую команду в php-контейнере
`./bin/console catalog:upload /path-to-file/xxx.epub`
1. Тестовые книги можно найти в репозитории, в разделе Downloads (https://bitbucket.org/crapty/agima_books_parsing/downloads/)