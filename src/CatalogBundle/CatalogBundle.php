<?php

namespace CatalogBundle;

use CatalogBundle\DependencyInjection\Compiler\BookParserCompilerPass;
use CatalogBundle\Service\BookParser\BookParserInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CatalogBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->registerForAutoconfiguration(BookParserInterface::class)
            ->addTag('catalog.book_parser');

        $container->addCompilerPass(new BookParserCompilerPass());
    }
}
