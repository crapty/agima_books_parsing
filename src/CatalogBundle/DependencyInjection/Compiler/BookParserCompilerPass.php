<?php

namespace CatalogBundle\DependencyInjection\Compiler;

use CatalogBundle\Service\BookParser\BookParser;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class BookParserCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $mainParser = $container->getDefinition(BookParser::class);

        $taggedServices = $container->findTaggedServiceIds('catalog.book_parser');

        $parsers = [];
        foreach ($taggedServices as $id => $tags) {
            if ($id !== BookParser::class) {
                $parsers[] = new Reference($id);
            }
        }

        $mainParser->setArgument(0, $parsers);
    }
}
