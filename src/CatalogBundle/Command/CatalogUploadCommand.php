<?php

namespace CatalogBundle\Command;

use CatalogBundle\Service\CatalogItemFileBuilderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CatalogUploadCommand extends Command
{
    /** @var CatalogItemFileBuilderInterface */
    protected $builder;

    /** @var EntityManagerInterface */
    protected $em;

    /**
     * CatalogUploadCommand constructor.
     * @param CatalogItemFileBuilderInterface $builder
     * @param EntityManagerInterface $em
     */
    public function __construct(CatalogItemFileBuilderInterface $builder, EntityManagerInterface $em)
    {
        parent::__construct();

        $this->builder = $builder;
        $this->em = $em;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->setName('catalog:upload')
            ->addArgument('f', InputArgument::REQUIRED, 'Filepath');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $filepath = (string)$input->getArgument('f');
            if (!file_exists($filepath)) {
                $output->writeln('file not found');

                return;
            }

            $filepathParts = explode('/', $filepath);
            $file = new UploadedFile($filepath, end($filepathParts));

            $catalogItem = $this->builder->buildFromFile($file);
        } catch (Exception $e) {
            $output->writeln($e->getMessage());

            return;
        }

        $this->em->persist($catalogItem);
        $this->em->flush();

        $output->writeln('Uploaded');
    }
}
