<?php

namespace CatalogBundle\Controller;

use CatalogBundle\Form\UploadRequestType;
use CatalogBundle\Model\UploadRequest;
use CatalogBundle\Service\CatalogItemFileBuilderInterface;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UploadController extends AbstractFOSRestController
{
    /**
     * @Route(path="/catalog/upload",methods={"GET","POST"},name="catalog_upload")
     *
     * @param Request $request
     * @param CatalogItemFileBuilderInterface $builder
     * @return Response
     */
    public function uploadAction(Request $request, CatalogItemFileBuilderInterface $builder): Response
    {
        $uploadReq = new UploadRequest();

        $form = $this->createForm(UploadRequestType::class, $uploadReq);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $catalogItem = $builder->buildFromFile($uploadReq->getFile());
            } catch (Exception $e) {
                return $this->handleView(
                    $this->view(['success' => false, 'errors' => [$e->getMessage()]], Response::HTTP_BAD_REQUEST)
                );
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($catalogItem);
            $em->flush();

            return $this->redirectToRoute('catalog_upload');
        }

        return $this->render(
            'upload.html.twig',
            ['form' => $form->createView()]
        );
    }
}
