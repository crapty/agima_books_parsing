<?php

namespace CatalogBundle\Controller;

use CatalogBundle\Form\UploadRequestApiType;
use CatalogBundle\Model\UploadRequest;
use CatalogBundle\Service\CatalogItemFileBuilderInterface;
use Doctrine\ORM\Query;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CatalogApiController extends AbstractFOSRestController
{
    /**
     * @Route(path="/api/v1/catalog/upload",methods={"POST"})
     *
     * @param Request $request
     * @param CatalogItemFileBuilderInterface $builder
     * @return Response
     */
    public function postUploadAction(Request $request, CatalogItemFileBuilderInterface $builder): Response
    {
        $uploadReq = new UploadRequest();

        $form = $this->createForm(UploadRequestApiType::class, $uploadReq);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $catalogItem = $builder->buildFromFile($uploadReq->getFile());
            } catch (Exception $e) {
                return $this->handleView(
                    $this->view(
                        ['code' => Response::HTTP_BAD_REQUEST, 'message' => $e->getMessage()],
                        Response::HTTP_BAD_REQUEST
                    )
                );
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($catalogItem);
            $em->flush();

            return $this->handleView($this->view(['code' => Response::HTTP_CREATED], Response::HTTP_CREATED));
        }

        return $this->handleView($this->view($form));
    }

    /**
     * @Route(path="/api/v1/catalog/books-per-day",methods={"GET"})
     *
     * @return Response
     */
    public function getUploadedPerDayAction(): Response
    {
        /** @var Query $query */
        $query = $this->getDoctrine()->getManager()->createQuery(
            <<<DQL
SELECT to_char(i.createdAt, 'YYYY-MM-DD') AS uploaded_at, COUNT(i.id) AS number FROM CatalogBundle\Entity\CatalogItem AS i
GROUP BY uploaded_at
DQL
        );

        return $this->handleView($this->view($query->getArrayResult()));
    }

    /**
     * @Route(path="/api/v1/catalog/books-per-author",methods={"GET"})
     *
     * @return Response
     */
    public function getBooksCountByAuthorAction(): Response
    {
        /** @var Query $query */
        $query = $this->getDoctrine()->getManager()->createQuery(
            <<<DQL
SELECT i.author, COUNT(i.id) AS qty FROM CatalogBundle\Entity\CatalogItem AS i
GROUP BY i.author
DQL
        );

        return $this->handleView($this->view($query->getArrayResult()));
    }

    /**
     * @Route(path="/api/v1/catalog/authors-without-books",methods={"GET"})
     *
     * @return Response
     */
    public function getAuthorsWithoutBooksAction(): Response
    {
        /*
         * TODO: Не смог до конца понять, зачем реализовывать данный эндпоинт:
         *  табличка же одна и без книг авторы туда никак не попадут.
         *  Если подразумевалось, что в бд должна быть отдельная таблица для авторов и связанная с ней таблица для книг,
         *  то в таком случае выполнение данного тестового задания сильно выходит за рамки оговоренных 2-4 часов.
         */

        /** @var Query $query */
        $query = $this->getDoctrine()->getManager()->createQuery(
            <<<DQL
SELECT i.author FROM CatalogBundle\Entity\CatalogItem AS i
GROUP BY i.author
HAVING COUNT(i.id) = 1
DQL
        );

        return $this->handleView($this->view(array_column($query->getScalarResult(), 'author')));
    }
}
