<?php

namespace CatalogBundle\Service\BookParser;

use CatalogBundle\Exception\BookParserException;
use CatalogBundle\Model\BookInfoInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface BookParserInterface
{
    /**
     * @param UploadedFile $file
     * @return BookInfoInterface
     * @throws BookParserException
     */
    public function parse(UploadedFile $file): BookInfoInterface;

    /**
     * @param UploadedFile $file
     * @return bool
     */
    public function isSupports(UploadedFile $file): bool;
}
