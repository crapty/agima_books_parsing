<?php

namespace CatalogBundle\Service\BookParser;

use CatalogBundle\Exception\BookParserException;
use CatalogBundle\Model\BookInfoInterface;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BookParser implements BookParserInterface
{
    /**
     * @var BookParserInterface[]
     */
    protected $parsers = [];

    /**
     * BookParser constructor.
     * @param BookParserInterface[] $parsers
     */
    public function __construct(array $parsers)
    {
        foreach ($parsers as $parser) {
            if (!$parser instanceof BookParserInterface) {
                throw new InvalidArgumentException(
                    sprintf("parser must be instance of %s", BookParserInterface::class)
                );
            }
        }

        $this->parsers = $parsers;
    }

    /**
     * @inheritDoc
     */
    public function parse(UploadedFile $file): BookInfoInterface
    {
        $parser = $this->findSuitableParser($file);
        if ($parser === null) {
            throw new BookParserException("Unsupported file");
        }

        return $parser->parse($file);
    }

    /**
     * @inheritDoc
     */
    public function isSupports(UploadedFile $file): bool
    {
        return $this->findSuitableParser($file) !== null;
    }

    private function findSuitableParser(UploadedFile $book): ?BookParserInterface
    {
        foreach ($this->parsers as $parser) {
            if ($parser->isSupports($book)) {
                return $parser;
            }
        }

        return null;
    }
}
