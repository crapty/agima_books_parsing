<?php

namespace CatalogBundle\Service\BookParser\Handler;

use CatalogBundle\Exception\BookParserException;
use CatalogBundle\Model\BookInfo;
use CatalogBundle\Model\BookInfoInterface;
use CatalogBundle\Service\BookParser\BookParserInterface;
use DOMDocument;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Fb2Parser implements BookParserInterface
{
    const FB2_MIME_TYPE = 'text/xml';
    const FB2_FILE_EXTENSION = '.fb2';

    /**
     * @inheritDoc
     */
    public function parse(UploadedFile $file): BookInfoInterface
    {
        $doc = new DOMDocument();
        $doc->strictErrorChecking = false;
        $doc->recover = true;
        if (!$doc->load($file->getPathname(), LIBXML_NOERROR)) {
            throw new BookParserException("Invalid .fb2 file");
        }

        $description = $doc->getElementsByTagName('description');
        $description = $description->item(0);
        if (!$description) {
            throw new BookParserException("Invalid .fb2 file");
        }

        $result = new BookInfo();

        $titleInfo = $description->getElementsByTagName('title-info')->item(0);

        $authorsList = $titleInfo->getElementsByTagName('author');
        foreach ($authorsList as $el) {
            $firstName = (string)$el->getElementsByTagName('first-name')->item(0)->nodeValue;
            $middleName = (string)$el->getElementsByTagName('middle-name')->item(0)->nodeValue;
            $lastName = (string)$el->getElementsByTagName('last-name')->item(0)->nodeValue;

            $result->setAuthor(trim(sprintf('%s %s %s', $firstName, $middleName, $lastName)));
        }

        $result->setTitle((string)$titleInfo->getElementsByTagName('book-title')->item(0)->nodeValue);
        $result->setLang((string)$description->getElementsByTagName('lang')->item(0)->nodeValue);

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function isSupports(UploadedFile $file): bool
    {
        if (self::FB2_MIME_TYPE !== $file->getMimeType()) {
            return false;
        }

        return self::FB2_FILE_EXTENSION === mb_substr($file->getClientOriginalName(), -4);
    }
}
