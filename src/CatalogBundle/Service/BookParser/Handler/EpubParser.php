<?php

namespace CatalogBundle\Service\BookParser\Handler;

use CatalogBundle\Exception\BookParserException;
use CatalogBundle\Model\BookInfo;
use CatalogBundle\Model\BookInfoInterface;
use CatalogBundle\Service\BookParser\BookParserInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ZipArchive;

class EpubParser implements BookParserInterface
{
    const EPUB_MIME_TYPE = 'application/epub+zip';

    /** @var ZipArchive */
    private $zipArchive;

    /**
     * EpubParser constructor.
     */
    public function __construct()
    {
        $this->zipArchive = new ZipArchive();
    }

    /**
     * @inheritDoc
     */
    public function parse(UploadedFile $file): BookInfoInterface
    {
        $ok = $this->zipArchive->open($file->getPathname());
        if ($ok !== true) {
            throw new BookParserException("Error: can't open epub file");
        }

        $buf = $this->getFileContentFromZipArchive("META-INF/container.xml");
        $opfContents = simplexml_load_string($buf);
        $opfAttributes = $opfContents->rootfiles->rootfile->attributes();
        $opfFile = (string)$opfAttributes->{'full-path'};

        $buf = $this->getFileContentFromZipArchive($opfFile);
        $opfContents = simplexml_load_string($buf);
        $meta = $opfContents->metadata->children('dc', true);

        $result = (new BookInfo())
            ->setAuthor((string)($meta->creator[0]))
            ->setTitle((string)($meta->title[0]))
            ->setLang((string)($meta->language[0]));

        $this->zipArchive->close();

        return $result;
    }

    private function getFileContentFromZipArchive($fileName): string
    {
        $fp = $this->zipArchive->getStream($fileName);
        if (!$fp) {
            throw new BookParserException("Error: can't get stream to epub file");
        }

        $buf = '';
        ob_start();
        while (!feof($fp)) {
            $buf .= fread($fp, 2048);
        }
        ob_end_clean();
        fclose($fp);

        return $buf;
    }

    public function isSupports(UploadedFile $file): bool
    {
        return self::EPUB_MIME_TYPE === $file->getMimeType();
    }
}
