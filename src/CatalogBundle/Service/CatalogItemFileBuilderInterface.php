<?php

namespace CatalogBundle\Service;

use CatalogBundle\Entity\CatalogItem;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface CatalogItemFileBuilderInterface
{
    /**
     * @param UploadedFile $file
     * @return CatalogItem
     * @throws Exception
     */
    public function buildFromFile(UploadedFile $file): CatalogItem;
}
