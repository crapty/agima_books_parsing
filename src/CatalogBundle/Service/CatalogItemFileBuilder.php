<?php

namespace CatalogBundle\Service;

use CatalogBundle\Entity\CatalogItem;
use CatalogBundle\Service\BookParser\BookParserInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CatalogItemFileBuilder implements CatalogItemFileBuilderInterface
{
    /** @var BookParserInterface */
    protected $parser;

    /** @var ValidatorInterface */
    protected $validator;

    /**
     * CatalogItemFileBuilder constructor.
     * @param BookParserInterface $parser
     * @param ValidatorInterface $validator
     */
    public function __construct(BookParserInterface $parser, ValidatorInterface $validator)
    {
        $this->parser = $parser;
        $this->validator = $validator;
    }

    /**
     * @inheritDoc
     */
    public function buildFromFile(UploadedFile $file): CatalogItem
    {
        $bookInfo = $this->parser->parse($file);

        $catalogItem = (new CatalogItem())
            ->setAuthor($bookInfo->getAuthor())
            ->setName($bookInfo->getTitle())
            ->setLang($bookInfo->getLang());

        $errors = $this->validator->validate($catalogItem);
        if (count($errors)) {
            throw new RuntimeException((string)$errors);
        }

        return $catalogItem;
    }
}
