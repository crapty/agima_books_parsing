<?php

namespace CatalogBundle\Form;

use CatalogBundle\Model\UploadRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UploadRequestApiType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, ['required' => true]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => UploadRequest::class,
                'csrf_protection' => false,
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
