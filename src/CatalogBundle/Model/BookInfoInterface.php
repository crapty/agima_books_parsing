<?php

namespace CatalogBundle\Model;

interface BookInfoInterface
{
    public function getAuthor(): string;

    /**
     * Book title
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Book language
     *
     * @return string
     */
    public function getLang(): string;
}
