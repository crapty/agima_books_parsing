<?php

namespace CatalogBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

class UploadRequest
{
    /**
     * @var UploadedFile|null
     *
     * @Assert\NotNull()
     * @Assert\File(
     *     maxSize="20m",
     *     mimeTypes={"application/epub+zip","text/xml"},
     *     mimeTypesMessage="Please upload a valid .epub/.fb2 document"
     * )
     */
    protected $file;

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile|null $file
     * @return UploadRequest
     */
    public function setFile(?UploadedFile $file): UploadRequest
    {
        $this->file = $file;

        return $this;
    }
}
