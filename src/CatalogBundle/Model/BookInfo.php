<?php

namespace CatalogBundle\Model;

class BookInfo implements BookInfoInterface
{
    /** @var string */
    protected $author = '';

    /** @var string */
    protected $title = '';

    /** @var string */
    protected $lang = '';

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return BookInfo
     */
    public function setAuthor(string $author): BookInfo
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return BookInfo
     */
    public function setTitle(string $title): BookInfo
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getLang(): string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return BookInfo
     */
    public function setLang(string $lang): BookInfo
    {
        $this->lang = $lang;

        return $this;
    }
}
