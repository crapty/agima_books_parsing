<?php

namespace CatalogBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="catalog_item")
 * @ORM\Entity(repositoryClass="CatalogBundle\Repository\CatalogItemRepository")
 */
class CatalogItem
{
    /**
     *
     * @var int|null
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    protected $author = '';

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    protected $name = '';

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $lang = '';

    /**
     * @var DateTime|null
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return CatalogItem
     */
    public function setAuthor(string $author): CatalogItem
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CatalogItem
     */
    public function setName(string $name): CatalogItem
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLang(): string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return CatalogItem
     */
    public function setLang(string $lang): CatalogItem
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime|null $createdAt
     * @return CatalogItem
     */
    public function setCreatedAt(?DateTime $createdAt): CatalogItem
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
